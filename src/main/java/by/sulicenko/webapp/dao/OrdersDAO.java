package by.sulicenko.webapp.dao;

import by.sulicenko.webapp.db.ProxyConnection;
import by.sulicenko.webapp.entity.Order;
import by.sulicenko.webapp.exception.DAOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class OrdersDAO extends AbstractDAO<Order> {
    private static final String ID = "id";
    private static final String CHECK_IN = "checkin";
    private static final String CHECK_OUT = "checkout";
    private static final String COST = "cost";
    private static final String ID_ROOM = "idroom";
    private static final String SELECT_ALL_SQL =
            "SELECT id checkin checkout cost idroom FROM orders";

    public OrdersDAO(ProxyConnection connection) {
        super(connection);
    }

    @Override
    public Set<Order> findAll() throws DAOException {
        HashSet<Order> hashSet;
        try {
            PreparedStatement ps = getConnection()
                    .prepareStatement(SELECT_ALL_SQL);
            ResultSet resultSet = ps.executeQuery();
            Order order = new Order();
            hashSet = new HashSet<>();

            while (resultSet.next()) {
                order.setId(resultSet.getLong(ID));
                order.setCheckIn(resultSet.getDate(CHECK_IN).toLocalDate());
                order.setCheckOut(resultSet.getDate(CHECK_OUT).toLocalDate());
                order.setCost(resultSet.getBigDecimal(COST));
                order.setIdRoom(resultSet.getLong(ID_ROOM));
                hashSet.add(order);
            }
            close(ps);
        } catch (SQLException e) {
            throw new DAOException("preparedStatement creation failed", e);
        }
        return hashSet;
    }

    @Override
    public boolean create(Order order) {
        return false;
    }

    @Override
    public boolean delete(Order order) {
        return false;
    }
}
