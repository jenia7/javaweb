package by.sulicenko.webapp.dao;

import by.sulicenko.webapp.db.ProxyConnection;
import by.sulicenko.webapp.entity.Entity;
import by.sulicenko.webapp.exception.DAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;
import java.util.Set;

public abstract class AbstractDAO <T extends Entity> {
    private static final Logger LOGGER = LogManager.getLogger();

    private ProxyConnection connection;

    protected AbstractDAO(ProxyConnection connection) {
        this.connection = connection;
    }

    public abstract Set<T> findAll() throws DAOException;

    public abstract boolean create(T entity);

    public abstract boolean delete(T entity);

    public ProxyConnection getConnection() {
        return connection;
    }

    public void close(Statement statement) {
        if (Objects.nonNull(statement)) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.log(Level.ERROR, "statement closing error");
            }
        }
    }
}
