package by.sulicenko.webapp.dao;

import by.sulicenko.webapp.db.ProxyConnection;
import by.sulicenko.webapp.entity.User;
import by.sulicenko.webapp.exception.DAOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class UsersDAO extends AbstractDAO<User> {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String NAME = "name";
    private static final String MONEY = "money";
    private static final String PHONE = "phone";
    private static final String SELECT_ALL_SQL =
            "SELECT login password name money phone FROM users";

    public UsersDAO(ProxyConnection connection) {
        super(connection);
    }

    @Override
    public Set<User> findAll() throws DAOException {
        HashSet<User> hashSet;
        try {
            PreparedStatement ps = getConnection()
                    .prepareStatement(SELECT_ALL_SQL);
            ResultSet rs = ps.executeQuery();
            User user = new User();
            hashSet = new HashSet<>();

            while (rs.next()) {
                user.setLogin(rs.getString(LOGIN));
                user.setPassword(rs.getString(PASSWORD));
                user.setName(rs.getString(NAME));
                user.setMoney(rs.getBigDecimal(MONEY));
                user.setPhone(rs.getString(PHONE));
                hashSet.add(user);
            }
            close(ps);
        } catch (SQLException e) {
            throw new DAOException("preparedStatement creation failed", e);
        }
        return hashSet;
    }

    @Override
    public boolean create(User user) {
        return false;
    }

    @Override
    public boolean delete(User user) {
        return false;
    }
}
