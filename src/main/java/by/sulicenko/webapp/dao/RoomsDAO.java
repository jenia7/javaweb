package by.sulicenko.webapp.dao;

import by.sulicenko.webapp.db.ProxyConnection;
import by.sulicenko.webapp.entity.Entity;
import by.sulicenko.webapp.entity.Order;
import by.sulicenko.webapp.entity.Room;
import by.sulicenko.webapp.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RoomsDAO extends AbstractDAO<Room> {
    private static final String ID_ROOM = "idroom";
    private static final String HOTEL = "hotel";
    private static final String PLACE = "place";
    private static final String COST_PER_DAY = "costpd";
    private static final String FREE = "isfree";
    private static final String SELECT_ALL_SQL =
            "SELECT idroom hotel place costpd isfree FROM rooms";

    public RoomsDAO(ProxyConnection connection) {
        super(connection);
    }

    @Override
    public Set<Room> findAll() throws DAOException {
        HashSet<Room> hashSet;
        try {
            PreparedStatement ps = getConnection()
                    .prepareStatement(SELECT_ALL_SQL);
            ResultSet resultSet = ps.executeQuery();
            Room room = new Room();
            hashSet = new HashSet<>();

            while (resultSet.next()) {
                room.setIdRoom(resultSet.getLong(ID_ROOM));
                room.setHotel(resultSet.getString(HOTEL));
                room.setPlace(resultSet.getByte(PLACE));
                room.setCostPerDay(resultSet.getBigDecimal(COST_PER_DAY));
                room.setFree(Boolean.valueOf(resultSet.getString(FREE)));//!!!!!!!
                hashSet.add(room);
            }
            close(ps);
        } catch (SQLException e) {
            throw new DAOException("preparedStatement creation failed", e);
        }
        return hashSet;
    }

    @Override
    public boolean create(Room room) {
        return false;
    }

    @Override
    public boolean delete(Room room) {
        return false;
    }
}
