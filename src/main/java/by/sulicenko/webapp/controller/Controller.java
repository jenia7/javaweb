package by.sulicenko.webapp.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Controller", urlPatterns = "/Controller")
public class Controller extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request,
                                  HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.log(Level.INFO, "in!!!!");
        //String servletVersion =  getServletContext().getServerInfo();
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        /*String encoding = request.getCharacterEncoding();

        GregorianCalendar gc = new GregorianCalendar();
        String timeJsp = request.getParameter("time");
        float delta = ((float)(gc.getTimeInMillis() - Long.parseLong(timeJsp))) / 1_000;
        request.setAttribute("res", delta);
        request.setAttribute("ch", encoding);*/
        request.getRequestDispatcher("/jsp/result.jsp").forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
        LOGGER.log(Level.DEBUG, "destroy servlet");

    }

    @Override
    public void init() throws ServletException {
        super.init();
        LOGGER.log(Level.DEBUG, "init servlet");
    }
}
