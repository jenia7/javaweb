package by.sulicenko.webapp.db;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final ConnectionPool pool = new ConnectionPool();
    private static final int WAITING_TIME = 7;

    private ArrayBlockingQueue<ProxyConnection> connectionQueue;

    private ConnectionPool() {
        ResourceBundle resource;
        DataSource dataSource;
        Properties properties;
        int poolSize;
        DataSourceFactory factory = new DataSourceFactory();

        try {
            resource = ResourceBundle.getBundle(ConnectionKey.DB);
            dataSource = factory.createDataSource(resource);
            poolSize = Integer.parseInt(
                    resource.getString(ConnectionKey.POOL_SIZE));
        } catch (MissingResourceException e) {
            properties = new Properties();
            properties.setProperty(ConnectionKey.URL, ConnectionKey.DEF_URL);
            properties.setProperty(ConnectionKey.USER, ConnectionKey.DEF_USER);
            properties.setProperty(ConnectionKey.PASSWORD,
                    ConnectionKey.DEF_PASSWORD);
            dataSource = factory.createDataSource(properties);
            poolSize = ConnectionKey.DEF_POOL_SIZE;
        }
        connectionQueue = new ArrayBlockingQueue<>(poolSize);
        ProxyConnection connection;

        for (int i = 0; i < poolSize; ++i) {
            try {
                connection = new ProxyConnection(dataSource.getConnection());
                connectionQueue.put(connection);

            } catch (SQLException | InterruptedException e) {
                LOGGER.log(Level.FATAL, "failed on connection pool creation");
                throw new RuntimeException("failed on connection pool creation", e);
            }
        }
    }

    public static ConnectionPool getPool() {
        return pool;
    }

    public ProxyConnection pollConnection() {
        ProxyConnection connection = null;
        try {
            connection = connectionQueue.poll(WAITING_TIME, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, "thread interruption while poll() is executing");
        }
        return connection;
    }

    public void closeConnection(ProxyConnection connection) {
        try {
            connectionQueue.put(connection);
        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, "thread interruption while put() is executing");
        }
    }
}
