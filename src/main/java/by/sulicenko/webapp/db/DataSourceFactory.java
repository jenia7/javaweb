package by.sulicenko.webapp.db;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import javax.sql.DataSource;
import java.util.Properties;
import java.util.ResourceBundle;

public class DataSourceFactory {

    public DataSource createDataSource(ResourceBundle bundle) {
        MysqlDataSource mysqlDataSource = new MysqlDataSource();

        mysqlDataSource.setURL(bundle.getString(ConnectionKey.URL));
        mysqlDataSource.setUser(bundle.getString(ConnectionKey.USER));
        mysqlDataSource.setPassword(bundle.getString(ConnectionKey.PASSWORD));

        return mysqlDataSource;
    }

    public DataSource createDataSource(Properties prop) {
        MysqlDataSource mysqlDataSource = new MysqlDataSource();

        mysqlDataSource.setURL(prop.getProperty(ConnectionKey.URL));
        mysqlDataSource.setUser(prop.getProperty(ConnectionKey.USER));
        mysqlDataSource.setPassword(prop.getProperty(ConnectionKey.PASSWORD));

        return mysqlDataSource;
    }
}
