package by.sulicenko.webapp.db;

public final class ConnectionKey {
    public static final String USER = "db.user";
    public static final String PASSWORD = "db.password";
    public static final String POOL_SIZE = "db.poolSize";
    public static final String URL = "db.url";
    public static final String DB = "database";
    public static final int DEF_POOL_SIZE = 7;
    public static final String DEF_PASSWORD = "7Va3N4b9";
    public static final String DEF_USER = "root";
    public static final String DEF_URL =
            "jdbc:mysql://localhost:3306/booking?useUnicode=true&characterEncoding=utf8";
}
