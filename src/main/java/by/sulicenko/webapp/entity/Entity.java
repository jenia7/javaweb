package by.sulicenko.webapp.entity;

public abstract class Entity {

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
