package by.sulicenko.webapp.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class Room extends Entity {
    private long idRoom;
    private String hotel;
    private byte place;
    private BigDecimal costPerDay;
    private boolean free;

    public Room() {}

    public Room(long idRoom, String hotel, byte place,
                BigDecimal costPerDay, boolean free) {
        this.idRoom = idRoom;
        this.hotel = hotel;
        this.place = place;
        this.costPerDay = costPerDay;
        this.free = free;
    }

    public long getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(long idRoom) {
        this.idRoom = idRoom;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public byte getPlace() {
        return place;
    }

    public void setPlace(byte place) {
        this.place = place;
    }

    public BigDecimal getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(BigDecimal costPerDay) {
        this.costPerDay = costPerDay;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRoom, hotel, place, costPerDay, free);
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (Objects.isNull(otherObject)) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        Room other = (Room) otherObject;
        return idRoom == other.idRoom
                && Objects.equals(hotel, other.hotel)
                && place == other.place
                && Objects.equals(costPerDay, other.costPerDay)
                && free == other.free;
    }
}
