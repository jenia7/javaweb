package by.sulicenko.webapp.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Order extends Entity {
    private long id;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private BigDecimal cost;
    private long idRoom;

    public Order() {}

    public Order(long id, LocalDate checkIn, LocalDate checkOut,
                 BigDecimal cost, long idRoom) {
        this.id = id;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.cost = cost;
        this.idRoom = idRoom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public long getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(long idRoom) {
        this.idRoom = idRoom;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, checkIn, checkOut, cost, idRoom);
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (Objects.isNull(otherObject)) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }

        Order other = (Order) otherObject;
        return id == other.id
                && Objects.equals(checkIn, other.checkIn)
                && Objects.equals(checkOut, other.checkOut)
                && Objects.equals(cost, other.cost)
                && idRoom == other.idRoom;
    }
}
