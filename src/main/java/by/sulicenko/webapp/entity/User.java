package by.sulicenko.webapp.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class User extends Entity {
    private String login;
    private String password;
    private String name;
    private BigDecimal money;
    private String phone;

    public User() {}

    public User(String login, String password,
                String name, BigDecimal money, String phone) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.money = money;
        this.phone = phone;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password, name, money, phone);
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (Objects.isNull(otherObject)) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        User other = (User) otherObject;
        return Objects.equals(login, other.login)
                && Objects.equals(password, other.password)
                && Objects.equals(name, other.name)
                && Objects.equals(money, other.money)
                && Objects.equals(phone, other.phone);
    }
}
