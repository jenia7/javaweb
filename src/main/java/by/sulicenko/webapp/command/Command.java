package by.sulicenko.webapp.command;

public interface Command {
    void execute();
}
