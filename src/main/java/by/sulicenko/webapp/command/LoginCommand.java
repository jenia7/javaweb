package by.sulicenko.webapp.command;

import by.sulicenko.webapp.logic.Receiver;

public class LoginCommand implements Command {
    private Receiver receiver;

    public LoginCommand() {}
    public LoginCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.action();
    }
}
