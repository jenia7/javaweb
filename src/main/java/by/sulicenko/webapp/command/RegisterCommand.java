package by.sulicenko.webapp.command;

import by.sulicenko.webapp.logic.Receiver;

public class RegisterCommand implements Command {
    private Receiver receiver;

    public RegisterCommand() {}

    public RegisterCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {

    }
}
