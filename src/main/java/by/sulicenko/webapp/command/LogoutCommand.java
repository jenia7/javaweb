package by.sulicenko.webapp.command;

import by.sulicenko.webapp.logic.Receiver;

public class LogoutCommand implements Command {
    private Receiver receiver;

    public LogoutCommand() {}
    public LogoutCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.action();
    }
}
