package by.sulicenko.webapp;

import by.sulicenko.webapp.dao.AbstractDAO;
import by.sulicenko.webapp.db.ConnectionPool;
import com.lambdaworks.crypto.SCryptUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SCryptTestDrive {
    public static void main1(String[] args) {
        String pass = "password";
        String hashed = SCryptUtil.scrypt(pass, 16, 16, 16);
        System.out.println(SCryptUtil.check(pass, hashed));
        System.out.println(hashed);
        System.out.println(hashed.length());
    }

    public static void main(String[] args) {
        String s = "INSERT INTO user (login, password, email) VALUES (?, ?, ?)";
        String sql = "SELECT id, login from user";
        Connection connection = ConnectionPool.getPool().pollConnection();
        try {
            PreparedStatement preparedStmt = connection.prepareStatement(s);
            //preparedStmt.setInt(1, 12);
            preparedStmt.setString(1, "Aлавaasd");
            preparedStmt.setString(2, "abra");
            preparedStmt.setString(3, "email");
            preparedStmt.executeUpdate();
            preparedStmt.close();

            preparedStmt = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStmt.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1)+ " " + resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
