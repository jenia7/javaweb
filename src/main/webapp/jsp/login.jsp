<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Login</title>
</head>
<body>
  <c:out value="hello superuser!"/>
  <c:set var="number" value="9.0" scope="page"></c:set>
  <c:if test="${number > 5.0}">
    <c:out value="number ${number} is higher then 5"/>
  </c:if>
  <form name="loginForm" method="post" action="Controller">
    <input type="hidden" name="command" value="login" />
    Login:<input name="login" value=""/>
    <br/>
    Password:<input type="password" name="password" value=""/>
    <br/>
    ${errorLoginPassMessage}
    <br/>
    ${wrongAction}
    <br/>
    ${nullPage}
    <br/>
    <input type="submit" value="Login"/>
  </form>
  <hr/>
</body>
</html>
